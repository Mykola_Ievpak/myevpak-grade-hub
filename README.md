## Used Technologies

1. Java 8
2. Spring DATA Jpa
3. Spring Security
4. Spring Web
5. H2 inmemory database
6. Swagger
7. Dozer
8. JUnit
9. Gradle

---

## Notes, what was not done but should be in prod like environments

1. No spring profiles used 
2. No data migration included (must be FlyWay or similar)
3. No logging included (should be used Logback and plugins in case project would like to use ELK stack)
4. No validation of input parameters, as not specified criterias
5. No customized Exception handling, used Spring native integrations.
6. Same schema/data was used for application and tests. In prod-like shoul be separated.
---


## Swagger

After application is started, you may access and Swagger UI via following link: http://localhost:8081/gradehub/v1/swagger-ui.html

API is divided for different tags, ralated to entity used: **Course, Exam**

Click on them to view available endpoints and their definitions.

Click on any endpoint and in right corner of the panel you will see "Try it button". You may click it, set input parameters and click Execute to see real requests, which will be shown as curl.

**Organization** ids available: **100-110**

**Course** ids available: **1000-1009**

**Exams** ids available: **1-7**

---

## H2 Database access
Onece application is started you may access Web interface for in-memory database via link: http://localhost:8081/gradehub/v1/h2-console

Input parameters:

**JDBC_url**: jdbc:h2:mem:testdb

**user**: sa

**password**: "leave empty it empty"

---


## Security

Spring basic authentication metchanizm is used. Simplest way to restrict access - based on HttpMethod used.

---

## Tests

SpringMvc is used to cover Controller and its endpoints. All endpoitns and behavior is covered, including different Http Statuses and responses.
JUnit is used as example for unit testing.

To run tests with recompilation (without caching) use command (gradle or gradlew depends on hot is installed): gradlew test -i -rerun-tasks
Console logging for Test task is changed to see progress and resulting number of failed, success and skipped tasks

---
