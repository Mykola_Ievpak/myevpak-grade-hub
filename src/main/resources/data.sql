INSERT INTO ORGANIZATION
(ID, NAME)
VALUES
(100,'Kyiv National Aviation University'),
(101,'Kyiv Boring School'),
(102,'Empty Organization for Test 1'),
(103,'Empty Organization for Test 2'),
(104,'Organization for Test 3'),
(105,'Organization for Delete Course Test'),
(106,'Organization for Patch Course Test'),
(107,'Organization for Get Exams Test'),
(108,'Organization for Post Exams Test'),
(109,'Organization for Get Exam Test'),
(110,'Organization for Delete Exam Test');

INSERT INTO COURSE
(ID, AUTHOR, ORGANIZATION_ID, TITLE)
VALUES
(1000,'Mykola Yevpak', 100, 'Best course you ever find'),
(1001,'Taras Shevchenko', 101, 'Can be fine for you'),
(1002,'Vasya Pupkin', 101, 'Well, just go there'),
(1003,'Vasya Pupkin', 104, 'Well, just go there'),
(1004,'Delete Course Test', 105, 'Delete Course Test'),
(1005,'Patch Course Test', 106, 'Patch Course Test'),
(1006,'Get Exams Test', 107, 'Get Exams Test'),
(1007,'Post Exams Test', 108, 'Post Exams Test'),
(1008,'Get Exam Test', 109, 'Get Exam Test'),
(1009,'Delete Exam Test', 110, 'Delete Exam Test');

INSERT INTO EXAM
(ID, COURSE_ID, INSTRUCTOR, TITLE)
VALUES
(1, 1000, 'Instructor A', 'Math'),
(2, 1000, 'Instructor B', 'Literature'),
(3, 1001, 'Instructor C', 'Physics'),
(4, 1003, 'Instructor Test 3', 'Test 3'),
(5, 1008, 'Instructor Test 4', 'Test 4'),
(6, 1009, 'Instructor Test 5', 'Test 5'),
(7, 1009, 'Instructor Test 5', 'Test 5');
