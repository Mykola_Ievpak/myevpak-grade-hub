package org.gradehub.api.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.dozer.Mapper;
import org.gradehub.api.dto.CourseDTO;
import org.gradehub.api.dto.ExamDTO;
import org.gradehub.api.repository.model.Course;
import org.gradehub.api.repository.model.Exam;
import org.gradehub.api.service.api.CourseService;
import org.gradehub.api.service.api.ExamService;
import org.gradehub.api.service.api.OrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Api(tags = {"GradeHub API"}, description = "Provide basic implementation of simple requests")
@RestController
@RequestMapping("/organizations")
public class OrganizationsController {

    @Autowired
    @Qualifier("QueryBasedCourseService")
    private CourseService courseService;
    @Autowired
    @Qualifier("QueryBasedExamService")
    private ExamService examService;
    @Autowired
    private OrganizationService organizationService;

    @Autowired
    private Mapper dozerMapper;

    @ApiOperation(value = "Get all available courses for organization", nickname = "getCourses", tags = "Course")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "List of courses provided"),
                    @ApiResponse(code = 404, message = "Resource not found", response = Map.class),
                    @ApiResponse(code = 403, message = "Access Denied", response = Map.class),
                    @ApiResponse(code = 401, message = "Unauthorized Access", response = Map.class)
            }
    )
    @GetMapping(path = "/{orgId}/courses")
    public ResponseEntity<Set<Course>> getCourses(@PathVariable("orgId") Long orgId) {

        Set<Course> courses = courseService.getCourses(orgId);

        return ResponseEntity.status(HttpStatus.OK).body(courses);
    }

    @ApiOperation(value = "Get course by id", nickname = "getCourse", tags = "Course")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Course found"),
                    @ApiResponse(code = 404, message = "Resource not found", response = Map.class),
                    @ApiResponse(code = 403, message = "Access Denied", response = Map.class),
                    @ApiResponse(code = 401, message = "Unauthorized Access", response = Map.class)
            }
    )
    @GetMapping(path = "/{orgId}/courses/{courseId}")
    public ResponseEntity<Course> getCourse(@PathVariable("orgId") Long orgId,
                                            @PathVariable("courseId") Long courseId) {

        Course course = courseService.getCourse(orgId, courseId);

        return ResponseEntity.status(HttpStatus.OK).body(course);
    }

    @ApiOperation(value = "Create course", nickname = "postCourse", tags = "Course")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 201, message = "Course added"),
                    @ApiResponse(code = 404, message = "Resource not found", response = Map.class),
                    @ApiResponse(code = 403, message = "Access Denied", response = Map.class),
                    @ApiResponse(code = 401, message = "Unauthorized Access", response = Map.class)
            }
    )
    @PostMapping("/{orgId}/courses")
    public ResponseEntity<Course> postCourse(@PathVariable("orgId") Long organizationId,
                                             @RequestBody CourseDTO courseDTO) {

        Course course = dozerMapper.map(courseDTO, Course.class);

        course.setOrganizationId(organizationId);

        //check if organization exists, to not have database constraints violation, but rather return 404 response
        organizationService.getOrganization(organizationId);

        Course coursePersisted = courseService.saveCourse(course);

        return ResponseEntity.status(HttpStatus.CREATED).body(coursePersisted);
    }

    @ApiOperation(value = "Delete course", nickname = "deleteCourse", tags = "Course")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Course added"),
                    @ApiResponse(code = 404, message = "Resource not found", response = Map.class),
                    @ApiResponse(code = 403, message = "Access Denied", response = Map.class),
                    @ApiResponse(code = 401, message = "Unauthorized Access", response = Map.class)
            }
    )
    @DeleteMapping("/{orgId}/courses/{courseId}")
    public ResponseEntity<?> deleteCourse(@PathVariable("orgId") Long organizationId,
                                          @PathVariable("courseId") Long courseId) {

        courseService.deleteCourse(organizationId, courseId);

        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @ApiOperation(value = "Patch any Course detail", nickname = "patchCourse", tags = "Course")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Course updated"),
                    @ApiResponse(code = 404, message = "Resource not found", response = Map.class),
                    @ApiResponse(code = 403, message = "Access Denied", response = Map.class),
                    @ApiResponse(code = 401, message = "Unauthorized Access", response = Map.class)
            }
    )
    @PatchMapping("/{orgId}/courses/{courseId}")
    public ResponseEntity<Course> patchCourse(@PathVariable("orgId") Long organizationId,
                                              @PathVariable("courseId") Long courseId,
                                              @RequestBody CourseDTO courseDTO) {

        Course updatedCourse = courseService.updateCourse(organizationId, courseId, courseDTO);

        return ResponseEntity.status(HttpStatus.OK).body(updatedCourse);
    }

    @ApiOperation(value = "Get all exams related to organization and course", nickname = "getExams", tags = "Exam")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Exam found"),
                    @ApiResponse(code = 404, message = "Resource not found", response = Map.class),
                    @ApiResponse(code = 403, message = "Access Denied", response = Map.class),
                    @ApiResponse(code = 401, message = "Unauthorized Access", response = Map.class)
            }
    )
    @GetMapping("/{orgId}/courses/{courseId}/exams")
    public ResponseEntity<List<Exam>> getExams(@PathVariable("orgId") Long organizationId,
                                              @PathVariable("courseId") Long courseId) {

        List<Exam> exams = examService.getExams(organizationId, courseId);

        return ResponseEntity.status(HttpStatus.OK).body(exams);
    }

    @ApiOperation(value = "Get exam within organization and course", nickname = "getExam", tags = "Exam")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Exam found"),
                    @ApiResponse(code = 404, message = "Resource not found", response = Map.class),
                    @ApiResponse(code = 403, message = "Access Denied", response = Map.class),
                    @ApiResponse(code = 401, message = "Unauthorized Access", response = Map.class)
            }
    )
    @GetMapping("/{orgId}/courses/{courseId}/exams/{examId}")
    public ResponseEntity<Exam> getExam(@PathVariable("orgId") Long organizationId,
                                        @PathVariable("courseId") Long courseId,
                                        @PathVariable("examId") Long examId) {

        Exam exam = examService.getExam(organizationId, courseId, examId);

        return ResponseEntity.status(HttpStatus.OK).body(exam);
    }

    @ApiOperation(value = "Delete exam within organization and course", nickname = "deleteExam", tags = "Exam")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Exam removed"),
                    @ApiResponse(code = 404, message = "Resource not found", response = Map.class),
                    @ApiResponse(code = 403, message = "Access Denied", response = Map.class),
                    @ApiResponse(code = 401, message = "Unauthorized Access", response = Map.class)
            }
     )
    @DeleteMapping("/{orgId}/courses/{courseId}/exams/{examId}")
    public ResponseEntity<Exam> deleteExam(@PathVariable("orgId") Long organizationId,
                                           @PathVariable("courseId") Long courseId,
                                           @PathVariable("examId") Long examId) {

        examService.deleteExam(organizationId, courseId, examId);

        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @ApiOperation(value = "Create exam", nickname = "postExam", tags = "Exam")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 201, message = "Exam created"),
                    @ApiResponse(code = 404, message = "Resource not found", response = Map.class),
                    @ApiResponse(code = 403, message = "Access Denied", response = Map.class),
                    @ApiResponse(code = 401, message = "Unauthorized Access", response = Map.class)
            }
    )
    @PostMapping("/{orgId}/courses/{courseId}/exams")
    public ResponseEntity<Exam> postExam(@PathVariable("orgId") Long organizationId,
                                         @PathVariable("courseId") Long courseId,
                                         @RequestBody ExamDTO examDTO) {

        Exam exam = dozerMapper.map(examDTO, Exam.class);
        exam.setCourseId(courseId);

        //check if organization exists, to not have database constraints violation, but rather return 404 response
        courseService.getCourse(organizationId, courseId);

        Exam persistedExam = examService.saveExam(exam);

        return ResponseEntity.status(HttpStatus.CREATED).body(persistedExam);
    }

}
