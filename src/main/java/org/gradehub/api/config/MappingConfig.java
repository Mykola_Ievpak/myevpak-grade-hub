package org.gradehub.api.config;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.dozer.loader.api.BeanMappingBuilder;
import org.gradehub.api.dto.CourseDTO;
import org.gradehub.api.dto.ExamDTO;
import org.gradehub.api.repository.model.Course;
import org.gradehub.api.repository.model.Exam;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static org.dozer.loader.api.TypeMappingOptions.mapEmptyString;
import static org.dozer.loader.api.TypeMappingOptions.mapNull;

@Configuration
public class MappingConfig {

    @Bean
    public Mapper getMapper () {
        DozerBeanMapper mapper = new DozerBeanMapper();
        mapper.addMapping(beanMappingBuilder());
        return mapper;
    }

    private BeanMappingBuilder beanMappingBuilder() {
        return new BeanMappingBuilder() {
            @Override
            protected void configure() {
                mapping(CourseDTO.class, Course.class, mapNull(false), mapEmptyString(false));
                mapping(ExamDTO.class, Exam.class, mapNull(false), mapEmptyString(false));
            }
        };
    }
}
