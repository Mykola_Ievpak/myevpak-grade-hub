package org.gradehub.api.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Collections;

@Configuration
public class ApiDocumentationConfiguration {

    @Bean
    public Docket apiDocumentation(@Value("${api-doc.host}") String host) {
        return new Docket(DocumentationType.SWAGGER_2).useDefaultResponseMessages(false).pathMapping("/")
                .apiInfo(metadata()).host(host).protocols(Collections.singleton("http"))
                .select()
                .apis(RequestHandlerSelectors.basePackage("org.gradehub.api"))
                .paths(PathSelectors.any()).build();
    }

    private ApiInfo metadata() {
        return new ApiInfoBuilder()
                .title("GradeHub API").description("Simple API implementation for test purpose").contact(
                        new Contact("", "http://www.gradehub.example.com",
                                "mykola.evpak@gmail.com;"))
                .version("1.0")
                .build();
    }
}
