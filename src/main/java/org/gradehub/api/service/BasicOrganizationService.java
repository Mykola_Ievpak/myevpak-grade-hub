package org.gradehub.api.service;

import org.gradehub.api.exception.ResourceNotFoundException;
import org.gradehub.api.repository.OrganizationRepository;
import org.gradehub.api.repository.model.Organization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BasicOrganizationService implements org.gradehub.api.service.api.OrganizationService {

    @Autowired
    private OrganizationRepository organizationRepository;

    @Override
    public Organization getOrganization(Long orgId) {

        return organizationRepository.findById(orgId)
                                     .orElseThrow(() -> new ResourceNotFoundException(String.format("Organization with id [%s] not found", orgId)));
    }
}
