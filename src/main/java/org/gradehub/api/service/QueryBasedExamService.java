package org.gradehub.api.service;

import org.gradehub.api.exception.ResourceNotFoundException;
import org.gradehub.api.repository.ExamRepository;
import org.gradehub.api.repository.model.Exam;
import org.gradehub.api.service.api.ExamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Qualifier("QueryBasedExamService")
public class QueryBasedExamService implements ExamService {

    @Autowired
    private ExamRepository examRepository;

    @Override
    public List<Exam> getExams(Long organizationId, Long courseId) {

        Optional<List<Exam>> exams = examRepository.findExamsByOrganizationAndCourseId(organizationId, courseId);

        if (!exams.isPresent()) {
            throw new ResourceNotFoundException(String.format("No exam found for organization [%s] and course [%s]", organizationId, courseId));
        }

        return exams.get();
    }

    @Override
    public Exam getExam(Long organizationId, Long courseId, Long examId) {

        Optional<Exam> exam = examRepository.findExam(organizationId, courseId, examId);

        if (!exam.isPresent()) {
            throw new ResourceNotFoundException(String.format("Exam with id [%s] not found for organization [%s] and course [%s]", examId, organizationId, courseId));
        }

        return exam.get();
    }

    @Override
    public void deleteExam(Long organizationId, Long courseId, Long examId) {
        Exam exam = getExam(organizationId, courseId, examId);

        examRepository.deleteExamById(exam.getId());
    }

    @Override
    public Exam saveExam(Exam exam) {
       return examRepository.saveAndFlush(exam);
    }

}
