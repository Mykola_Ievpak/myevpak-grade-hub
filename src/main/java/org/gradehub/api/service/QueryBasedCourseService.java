package org.gradehub.api.service;

import org.dozer.Mapper;
import org.gradehub.api.dto.CourseDTO;
import org.gradehub.api.exception.ResourceNotFoundException;
import org.gradehub.api.repository.CourseRepository;
import org.gradehub.api.repository.model.Course;
import org.gradehub.api.service.api.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
@Qualifier("QueryBasedCourseService")
public class QueryBasedCourseService implements CourseService {

    @Autowired
    private CourseRepository courseRepository;
    @Autowired
    private Mapper dozerMapper;

    @Override
    public Set<Course> getCourses(Long organizationId) {

        Set<Course> courses = courseRepository.findAllByOrganizationId(organizationId);

        if (courses == null || courses.size() == 0) {
            throw new ResourceNotFoundException(String.format("Courses for organization with id [%s] not found", organizationId));
        }

        return courses;
    }

    @Override
    public Course getCourse(Long organizationId, Long courseId) {

        Optional<Course> course = courseRepository.findAllByOrganizationIdAndId(organizationId, courseId);

        if(!course.isPresent()) {
            throw new ResourceNotFoundException(String.format("Course with id [%s] not found for organization [%s]", courseId, organizationId));
        }

        return course.get();
    }

    @Override
    public Course saveCourse(Course course) {
        return courseRepository.saveAndFlush(course);
    }

    @Override
    public void deleteCourse(Long organizationId, Long courseId) {

        Course course = getCourse(organizationId, courseId);
        courseRepository.delete(course);
    }

    @Override
    public Course updateCourse(Long organizationId, Long courseId, CourseDTO courseDTO) {

        Course course = getCourse(organizationId,  courseId);
        dozerMapper.map(courseDTO, course);

        return courseRepository.saveAndFlush(course);
    }

}
