package org.gradehub.api.service.api;

import org.gradehub.api.dto.CourseDTO;
import org.gradehub.api.repository.model.Course;

import java.util.Set;

public interface CourseService {
    Set<Course> getCourses(Long organizationId);

    Course getCourse(Long organizationId, Long courseId);

    Course saveCourse(Course course);

    void deleteCourse(Long organizationId, Long courseId);

    Course updateCourse(Long organizationId, Long courseId, CourseDTO courseDTO);
}
