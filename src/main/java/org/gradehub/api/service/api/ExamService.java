package org.gradehub.api.service.api;

import org.gradehub.api.repository.model.Exam;

import java.util.List;

public interface ExamService {

    List<Exam> getExams(Long organizationId, Long courseId);

    Exam getExam(Long organizationId, Long courseId, Long examId);

    void deleteExam(Long organizationId, Long courseId, Long examId);

    Exam saveExam(Exam exam);
}
