package org.gradehub.api.service.api;

import org.gradehub.api.repository.model.Organization;

public interface OrganizationService {
    Organization getOrganization(Long orgId);
}
