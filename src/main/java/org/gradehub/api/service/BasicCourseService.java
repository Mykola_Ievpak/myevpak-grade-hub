package org.gradehub.api.service;

import org.dozer.Mapper;
import org.gradehub.api.dto.CourseDTO;
import org.gradehub.api.exception.ResourceNotFoundException;
import org.gradehub.api.repository.CourseRepository;
import org.gradehub.api.repository.model.Course;
import org.gradehub.api.service.api.CourseService;
import org.gradehub.api.service.api.OrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
@Qualifier("BasicCourseService")
public class BasicCourseService implements CourseService {

    @Autowired
    private CourseRepository courseRepository;
    @Autowired
    private OrganizationService organizationService;
    @Autowired
    private Mapper dozerMapper;

    @Override
    public Set<Course> getCourses(Long organizationId) {

        Set<Course> courses = organizationService.getOrganization(organizationId).getCourses();

        if (courses.size() == 0)
            throw new ResourceNotFoundException(String.format("Courses for organization with id [%s] not found", organizationId));

        return courses;
    }

    @Override
    public Course getCourse(Long organizationId, Long courseId) {

        Optional<Course> course = getCourses(organizationId).stream().filter(c -> c.getId().equals(courseId)).findFirst();

        if(!course.isPresent()) {
            throw new ResourceNotFoundException(String.format("Course with id [%s] not found for organization [%s]", courseId, organizationId));
        }

        return course.get();
    }

    @Override
    public Course saveCourse(Course course) {
        organizationService.getOrganization(course.getOrganizationId());

        return courseRepository.save(course);
    }

    @Override
    public void deleteCourse(Long organizationId, Long courseId) {
        Course course = getCourse(organizationId, courseId);

        courseRepository.delete(course);
    }

    @Override
    public Course updateCourse(Long organizationId, Long courseId, CourseDTO courseDTO) {

        Course course = getCourse(organizationId,  courseId);

        dozerMapper.map(courseDTO, course);

        return courseRepository.saveAndFlush(course);
    }

}
