package org.gradehub.api.service;

import org.gradehub.api.exception.ResourceNotFoundException;
import org.gradehub.api.repository.ExamRepository;
import org.gradehub.api.repository.model.Course;
import org.gradehub.api.repository.model.Exam;
import org.gradehub.api.service.api.CourseService;
import org.gradehub.api.service.api.ExamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Qualifier("BasicExamService")
public class BasicExamService implements ExamService {

    @Autowired
    private ExamRepository examRepository;
    @Autowired
    @Qualifier("BasicCourseService")
    private CourseService courseService;

    @Override
    public List<Exam> getExams(Long organizationId, Long courseId) {

        return courseService.getCourse(organizationId, courseId).getExams();
    }

    @Override
    public Exam getExam(Long organizationId, Long courseId, Long examId) {

        Course course = courseService.getCourse(organizationId, courseId);

        Optional<Exam> exam = course.getExams().stream().filter(e -> e.getId().equals(examId)).findFirst();

        if (!exam.isPresent()) {
            throw new ResourceNotFoundException(String.format("Exam with id [%s] not found for organization [%s] and course [%s]", examId, organizationId, courseId));
        }

        return exam.get();
    }

    @Override
    public void deleteExam(Long organizationId, Long courseId, Long examId) {
        Exam exam = getExam(organizationId, courseId, examId);

        examRepository.deleteExamById(exam.getId());
    }

    @Override
    public Exam saveExam(Exam exam) {
        return examRepository.saveAndFlush(exam);
    }
}
