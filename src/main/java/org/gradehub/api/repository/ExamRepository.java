package org.gradehub.api.repository;

import org.gradehub.api.repository.model.Exam;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface ExamRepository extends JpaRepository<Exam, Long> {

    @Transactional
    @Modifying
    @Query(value = "DELETE FROM Exam e where e.id = ?1")
    void deleteExamById(Long examId);

    @Query(value = "SELECT e FROM Exam e " +
            "INNER JOIN Course c ON e.courseId = c.id " +
            "INNER JOIN Organization o ON c.organizationId = o.id " +
            "where " +
            "e.id = ?3 " +
            "and c.id = ?2 " +
            "and o.id = ?1 ")
    Optional<Exam> findExam(Long organizationId, Long courseId, Long examId);

    @Query(value = "SELECT e FROM Exam e " +
            "INNER JOIN Course c ON e.courseId = c.id " +
            "INNER JOIN Organization o ON c.organizationId = o.id " +
            "where " +
            "o.id = ?1 " +
            "and c.id = ?2 ")
    Optional<List<Exam>> findExamsByOrganizationAndCourseId(Long organizationId, Long courseId);
}
