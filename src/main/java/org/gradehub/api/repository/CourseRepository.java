package org.gradehub.api.repository;

import org.gradehub.api.repository.model.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {

    Set<Course> findAllByOrganizationId(Long organizationId);

    Optional<Course> findAllByOrganizationIdAndId(Long organizationId, Long courseId);

}
