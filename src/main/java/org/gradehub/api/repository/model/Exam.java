package org.gradehub.api.repository.model;

import javax.persistence.*;

@Entity
@Table(name="EXAM")
public class Exam {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "COURSE_ID")
    private Long courseId;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "INSTRUCTOR")
    private String instructor;

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInstructor() {
        return instructor;
    }

    public void setInstructor(String instructor) {
        this.instructor = instructor;
    }


    public Exam () {
        //no sonar
    }

    public Exam(Long id, Long courseId, String title, String instructor) {
        this.id = id;
        this.courseId = courseId;
        this.title = title;
        this.instructor = instructor;
    }

}
