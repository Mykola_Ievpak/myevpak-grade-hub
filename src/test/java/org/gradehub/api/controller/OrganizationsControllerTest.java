package org.gradehub.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.gradehub.api.dto.CourseDTO;
import org.gradehub.api.dto.ExamDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class OrganizationsControllerTest {

    private static Long NON_EXISTING_ORGANIZATION = 666L;
    private static Long NON_EXISTING_COURSE = 666L;
    private static Long NON_EXISTING_EXAM = 666L;

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void shouldGetListOfCourses() throws Exception {
        this.mockMvc.perform(get("/organizations/101/courses").with(httpBasic("admin", "admin")))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$[0].id").value("1001"))
                    .andExpect(jsonPath("$[0].title").value("Can be fine for you"))
                    .andExpect(jsonPath("$[0].author").value("Taras Shevchenko"))
                    .andExpect(jsonPath("$[0].organizationId").value("101"))
                    .andExpect(jsonPath("$[0].exams.[0].id").value("3"))
                    .andExpect(jsonPath("$[0].exams.[0].courseId").value("1001"))
                    .andExpect(jsonPath("$[0].exams.[0].title").value("Physics"))
                    .andExpect(jsonPath("$[0].exams.[0].instructor").value("Instructor C"))
                    .andDo(print());
    }

    @Test
    public void shouldReturnNotFoundIfOrganizationDoesNotExist() throws Exception {
        this.mockMvc.perform(get("/organizations/" + NON_EXISTING_ORGANIZATION + "/courses").with(httpBasic("admin", "admin")))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldReturnNotFoundIfCoursesWereNotFoundWithinOrganization() throws Exception {
        this.mockMvc.perform(get("/organizations/103/courses").with(httpBasic("admin", "admin")))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldCreateNewCourseWithinOrganization() throws Exception {
        CourseDTO course = new CourseDTO();
        course.setAuthor("New Author");
        course.setTitle("New Title");

        this.mockMvc.perform(post("/organizations/102/courses")
                             .with(httpBasic("admin", "admin"))
                             .contentType(MediaType.APPLICATION_JSON)
                             .content(objectMapper.writeValueAsString(course)))
                    .andExpect(status().isCreated())
                    .andExpect(jsonPath("$.id").isNotEmpty())
                    .andDo(print());
    }

    @Test
    public void shouldReturnNotFoundIfOrganizationDoesNotExistDuringPostCourse() throws Exception {
        CourseDTO course = new CourseDTO();
        course.setAuthor("New Author");
        course.setTitle("New Title");

        this.mockMvc.perform(post("/organizations/" + NON_EXISTING_ORGANIZATION + "/courses")
                .with(httpBasic("admin", "admin"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(course)))
                .andExpect(status().isNotFound())
                .andDo(print());
    }

    @Test
    public void shouldReturnOneCourseWithinOrganization() throws Exception {
        this.mockMvc.perform(get("/organizations/104/courses/1003").with(httpBasic("admin", "admin")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1003"))
                .andExpect(jsonPath("$.title").value("Well, just go there"))
                .andExpect(jsonPath("$.author").value("Vasya Pupkin"))
                .andExpect(jsonPath("$.organizationId").value("104"))
                .andExpect(jsonPath("$.exams.[0].id").value("4"))
                .andExpect(jsonPath("$.exams.[0].courseId").value("1003"))
                .andExpect(jsonPath("$.exams.[0].title").value("Test 3"))
                .andExpect(jsonPath("$.exams.[0].instructor").value("Instructor Test 3"));
    }

    @Test
    public void shouldReturnNotFoundIfCourseDoesNotExistWithinOrganization() throws Exception {
        this.mockMvc.perform(get("/organizations/101/courses/"+ NON_EXISTING_COURSE).with(httpBasic("admin", "admin")))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldDeleteCourseFromOrganization() throws Exception {
        this.mockMvc.perform(delete("/organizations/105/courses/1004").with(httpBasic("admin", "admin")))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldReturnNotFoundIfCourseToDeleteDoesNotExist() throws Exception {
        this.mockMvc.perform(delete("/organizations/105/courses/1005").with(httpBasic("admin", "admin")))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldReturnNotFoundIfOrganizationDoesNotExistDuringDeleteCourseOperation() throws Exception {
        this.mockMvc.perform(delete("/organizations/"+ NON_EXISTING_ORGANIZATION + "/courses/1005").with(httpBasic("admin", "admin")))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldReturnUnauthorizedIfDeleteOperationIsPerformedByUnauthorizedUser() throws Exception {
        this.mockMvc.perform(delete("/organizations/"+ NON_EXISTING_ORGANIZATION + "/courses/1005").with(httpBasic("non_existing_user", "non_existing_user")))
                .andExpect(status().isUnauthorized());
    }


    @Test
    public void shouldPatchCourseWitWithAuthorOnly() throws Exception {
        CourseDTO course = new CourseDTO();
        course.setAuthor("New Title From Test");

        this.mockMvc.perform(patch("/organizations/106/courses/1005")
                             .with(httpBasic("admin", "admin"))
                             .contentType(MediaType.APPLICATION_JSON)
                             .content(objectMapper.writeValueAsString(course)))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id").value("1005"))
                    .andExpect(jsonPath("$.title").value("Patch Course Test"))
                    .andExpect(jsonPath("$.author").value("New Title From Test"))
                    .andExpect(jsonPath("$.organizationId").value("106"));
    }

    @Test
    public void shouldReturnNotFoundIfCourseWasNotFoundForPatchOperation() throws Exception {
        CourseDTO course = new CourseDTO();
        course.setAuthor("New Title From Test");

        this.mockMvc.perform(patch("/organizations/106/courses/" + NON_EXISTING_COURSE)
                             .with(httpBasic("admin", "admin"))
                             .contentType(MediaType.APPLICATION_JSON)
                             .content(objectMapper.writeValueAsString(course)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldReturnUnauthorizedForPatchOperation() throws Exception {
        CourseDTO course = new CourseDTO();
        course.setAuthor("New Title From Test");

        this.mockMvc.perform(patch("/organizations/106/courses/" + NON_EXISTING_COURSE)
                .with(httpBasic("non_existing_user", "non_existing_user"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(course)))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void shouldGetAllAvailableExamsForOrganizationAndCourse() throws Exception {
        this.mockMvc.perform(get("/organizations/100/courses/1000/exams").with(httpBasic("admin", "admin")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value("1"))
                .andExpect(jsonPath("$[0].courseId").value("1000"))
                .andExpect(jsonPath("$[0].title").value("Math"))
                .andExpect(jsonPath("$[0].instructor").value("Instructor A"))
                .andExpect(jsonPath("$[1].id").value("2"))
                .andExpect(jsonPath("$[1].courseId").value("1000"))
                .andExpect(jsonPath("$[1].title").value("Literature"))
                .andExpect(jsonPath("$[1].instructor").value("Instructor B"));
    }

    @Test
    public void shouldReturnNotFoundIfOrganizationNotFoundForGetExamsOperation() throws Exception {
        this.mockMvc.perform(get("/organizations/" + NON_EXISTING_ORGANIZATION + "/courses/1000/exams").with(httpBasic("admin", "admin")))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldReturnNotFoundIfCourseNotFoundForGetExamsOperation() throws Exception {
        this.mockMvc.perform(get("/organizations/100/courses/" + NON_EXISTING_COURSE + "/exams").with(httpBasic("admin", "admin")))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldReturnNotFoundIfExamsWereNotFoundForGetExamsOperation() throws Exception {
        this.mockMvc.perform(get("/organizations/107/courses/1006/exams").with(httpBasic("admin", "admin")))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldReturnUnauthorizedForGetExamsOperation() throws Exception {
        this.mockMvc.perform(get("/organizations/107/courses/1006/exams").with(httpBasic("non_existing_user", "non_existing_user")))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void shouldCreateNewExamToOrganizationAndCourse() throws Exception {
        ExamDTO exam = new ExamDTO();
        exam.setInstructor("Instructor");
        exam.setTitle("Title");

        this.mockMvc.perform(post("/organizations/108/courses/1007/exams")
                .with(httpBasic("admin", "admin"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(exam)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").isNotEmpty())
                .andExpect(jsonPath("$.courseId").value("1007"))
                .andExpect(jsonPath("$.title").value("Title"))
                .andExpect(jsonPath("$.instructor").value("Instructor"));
    }

    @Test
    public void shouldReturnNotFoundIfOrganizationNotFoundForPostExamOperation() throws Exception {
        ExamDTO exam = new ExamDTO();
        exam.setInstructor("Instructor");
        exam.setTitle("Title");

        this.mockMvc.perform(post("/organizations/" + NON_EXISTING_ORGANIZATION + "/courses/1007/exams")
                .with(httpBasic("admin", "admin"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(exam)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldReturnNotFoundIfCourseNotFoundForPostExamOperation() throws Exception {
        ExamDTO exam = new ExamDTO();
        exam.setInstructor("Instructor");
        exam.setTitle("Title");

        this.mockMvc.perform(post("/organizations/108/courses/" + NON_EXISTING_COURSE + "/exams")
                .with(httpBasic("admin", "admin"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(exam)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldReturnUnauthorizedForPostExamOperation() throws Exception {
        this.mockMvc.perform(post("/organizations/108/courses/1007/exams")
                .with(httpBasic("non_existing_user", "non_existing_user")))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void shouldReturnExamInfoForGetExamOperation() throws Exception {
        this.mockMvc.perform(get("/organizations/109/courses/1008/exams/5").with(httpBasic("admin", "admin")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("5"))
                .andExpect(jsonPath("$.courseId").value("1008"))
                .andExpect(jsonPath("$.title").value("Test 4"))
                .andExpect(jsonPath("$.instructor").value("Instructor Test 4"));
    }

    @Test
    public void shouldReturnNotFoundIfOrganizationNotFoundForGetExamOperation() throws Exception {
        this.mockMvc.perform(get("/organizations/"+ NON_EXISTING_ORGANIZATION +"/courses/1008/exams/5").with(httpBasic("admin", "admin")))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldReturnNotFoundIfCourseNotFoundForGetExamOperation() throws Exception {
        this.mockMvc.perform(get("/organizations/109/courses/"+ NON_EXISTING_COURSE +"/exams/5").with(httpBasic("admin", "admin")))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldReturnUnauthorizedForGetExamOperation() throws Exception {
        this.mockMvc.perform(get("/organizations/109/courses/10008/exams/5").with(httpBasic("non_existing_user", "non_existing_user")))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void shouldDeleteExamFromOrganizationAndCourse() throws Exception {
        this.mockMvc.perform(delete("/organizations/110/courses/1009/exams/6").with(httpBasic("admin", "admin")))
                .andExpect(status().isOk());

        this.mockMvc.perform(get("/organizations/110/courses/1009/exams/6").with(httpBasic("admin", "admin")))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldReturnNotFoundIfOrganizationDoesNotExistForDeleteExamOperation() throws Exception {
        this.mockMvc.perform(delete("/organizations/"+ NON_EXISTING_ORGANIZATION +"/courses/1009/exams/6").with(httpBasic("admin", "admin")))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldReturnNotFoundIfCourseDoesNotExistForDeleteExamOperation() throws Exception {
        this.mockMvc.perform(delete("/organizations/110/courses/"+NON_EXISTING_COURSE+"/exams/6").with(httpBasic("admin", "admin")))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldReturnNotFoundExamDoesNotExistForDeleteExamOperation() throws Exception {
        this.mockMvc.perform(delete("/organizations/110/courses/1009/exams/" + NON_EXISTING_EXAM).with(httpBasic("admin", "admin")))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldReturnUnauthorizedForDeleteExamOperation() throws Exception {
        this.mockMvc.perform(delete("/organizations/110/courses/1009/exams/6").with(httpBasic("non_existing_user", "non_existing_user")))
                .andExpect(status().isUnauthorized());
    }

}