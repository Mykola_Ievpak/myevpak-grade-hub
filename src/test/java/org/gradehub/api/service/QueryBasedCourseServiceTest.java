package org.gradehub.api.service;

import org.dozer.Mapper;
import org.gradehub.api.dto.CourseDTO;
import org.gradehub.api.exception.ResourceNotFoundException;
import org.gradehub.api.repository.CourseRepository;
import org.gradehub.api.repository.model.Course;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
@RunWith(MockitoJUnitRunner.class)
public class QueryBasedCourseServiceTest {

    private static final long ORGANIZATION_ID = 2L;
    private static final long COURSE_ID = 3L;

    @InjectMocks
    private QueryBasedCourseService underTestInstance;

    @Mock
    private CourseRepository courseRepository;
    @Mock
    private Mapper dozerMapper;

    private Course courseMath = new Course(1L, "Title", "Author", ORGANIZATION_ID, null);
    private Course coursePersisted = new Course(2L, "Title2", "Author2", ORGANIZATION_ID, null);

    private Set<Course> courses = new HashSet<>(Arrays.asList(courseMath, coursePersisted));

    @Test
    public void shouldReturnAllCourses() {
        when(courseRepository.findAllByOrganizationId(ORGANIZATION_ID)).thenReturn(courses);

        Set<Course> result = underTestInstance.getCourses(ORGANIZATION_ID);

        verify(courseRepository, times(1)).findAllByOrganizationId(ORGANIZATION_ID);
        assertEquals(courses, result);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void shouldReturnNotFoundExceptionWhenNoCoursesAvailable() {
        when(courseRepository.findAllByOrganizationId(ORGANIZATION_ID)).thenReturn(null);

        underTestInstance.getCourses(ORGANIZATION_ID);

        verify(courseRepository, times(1)).findAllByOrganizationId(ORGANIZATION_ID);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void shouldReturnNotFoundExceptionWhenNoCoursesAvailable2() {
        when(courseRepository.findAllByOrganizationId(ORGANIZATION_ID)).thenReturn(new HashSet<>());

        underTestInstance.getCourses(ORGANIZATION_ID);

        verify(courseRepository, times(1)).findAllByOrganizationId(ORGANIZATION_ID);
    }

    @Test
    public void shouldReturnOneCourse() {
        when(courseRepository.findAllByOrganizationIdAndId(ORGANIZATION_ID, COURSE_ID)).thenReturn(Optional.of(courseMath));

        Course result = underTestInstance.getCourse(ORGANIZATION_ID, COURSE_ID);

        verify(courseRepository, times(1)).findAllByOrganizationIdAndId(ORGANIZATION_ID, COURSE_ID);
        assertEquals(courseMath, result);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void shouldThrowNotFoundExceptionIfNoCourseFound() {
        when(courseRepository.findAllByOrganizationIdAndId(ORGANIZATION_ID, COURSE_ID)).thenReturn(Optional.empty());

        underTestInstance.getCourse(ORGANIZATION_ID, COURSE_ID);

        verify(courseRepository, times(1)).findAllByOrganizationIdAndId(ORGANIZATION_ID, COURSE_ID);
    }

    @Test
    public void shouldSaveCourse() {
        when(courseRepository.saveAndFlush(coursePersisted)).thenReturn(coursePersisted);

        Course result = underTestInstance.saveCourse(coursePersisted);

        verify(courseRepository, times(1)).saveAndFlush(coursePersisted);

        assertEquals(coursePersisted, result);
    }

    @Test
    public void shouldDeleteCourseByOrganizationAndCourseId() {
        when(courseRepository.findAllByOrganizationIdAndId(ORGANIZATION_ID, COURSE_ID)).thenReturn(Optional.of(courseMath));

        underTestInstance.deleteCourse(ORGANIZATION_ID, COURSE_ID);

        verify(courseRepository, times(1)).delete(courseMath);
    }

    @Test
    public void shouldUpdateCourse() {
        CourseDTO dto = new CourseDTO();
        when(courseRepository.findAllByOrganizationIdAndId(ORGANIZATION_ID, COURSE_ID)).thenReturn(Optional.of(courseMath));

        underTestInstance.updateCourse(ORGANIZATION_ID, COURSE_ID, new CourseDTO());

        verify(courseRepository, times(1)).saveAndFlush(courseMath);
    }

}