package org.gradehub.api.service;

import org.gradehub.api.exception.ResourceNotFoundException;
import org.gradehub.api.repository.ExamRepository;
import org.gradehub.api.repository.model.Exam;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class QueryBasedExamServiceTest {

    private static final long ORGANIZATION_ID = 1111L;
    private static final long COURSE_ID = 2222L;
    private static final long EXAM_ID = 3333L;

    @InjectMocks
    private QueryBasedExamService underTestInstance;

    @Mock
    private ExamRepository examRepository;

    private Exam examMath = new Exam(EXAM_ID, 1L, "T1", "T1");
    private Exam persistedExam = new Exam(1L, 1L, "T1", "T1");
    private List<Exam> exams = Arrays.asList(examMath, persistedExam);

    @Test
    public void shouldGetListOfExams() {
        when(examRepository.findExamsByOrganizationAndCourseId(ORGANIZATION_ID, COURSE_ID)).thenReturn(Optional.of(exams));

        List<Exam> result = underTestInstance.getExams(ORGANIZATION_ID, COURSE_ID);

        verify(examRepository, times(1)).findExamsByOrganizationAndCourseId(ORGANIZATION_ID, COURSE_ID);

        assertEquals(exams, result);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void shouldThrowResourceNotFoundExceptionIfExamsNotFound() {
        when(examRepository.findExamsByOrganizationAndCourseId(ORGANIZATION_ID, COURSE_ID)).thenReturn(Optional.empty());

        underTestInstance.getExams(ORGANIZATION_ID, COURSE_ID);

        verify(examRepository, times(1)).findExamsByOrganizationAndCourseId(ORGANIZATION_ID, COURSE_ID);
    }

    @Test
    public void shouldFindOneExam() {
        when(examRepository.findExam(ORGANIZATION_ID, COURSE_ID, EXAM_ID)).thenReturn(Optional.of(examMath));

        Exam result = underTestInstance.getExam(ORGANIZATION_ID, COURSE_ID, EXAM_ID);

        verify(examRepository, times(1)).findExam(ORGANIZATION_ID, COURSE_ID, EXAM_ID);
        assertEquals(examMath, result);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void shouldThrowNotFoundExceptionIfExamNotFound() {
        when(examRepository.findExam(ORGANIZATION_ID, COURSE_ID, EXAM_ID)).thenReturn(Optional.empty());

        underTestInstance.getExam(ORGANIZATION_ID, COURSE_ID, EXAM_ID);

        verify(examRepository, times(1)).findExam(ORGANIZATION_ID, COURSE_ID, EXAM_ID);
    }

    @Test
    public void deleteExam() {
        when(examRepository.findExam(ORGANIZATION_ID, COURSE_ID, EXAM_ID)).thenReturn(Optional.of(examMath));

        underTestInstance.deleteExam(ORGANIZATION_ID, COURSE_ID, EXAM_ID);

        verify(examRepository, times(1)).deleteExamById(EXAM_ID);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void shouldThrowNotFoundExceptionForDeleteExamOperation() {
        when(examRepository.findExam(ORGANIZATION_ID, COURSE_ID, EXAM_ID)).thenReturn(Optional.empty());

        underTestInstance.deleteExam(ORGANIZATION_ID, COURSE_ID, EXAM_ID);

        verify(examRepository, times(1)).deleteExamById(EXAM_ID);
    }

    @Test
    public void saveExam() {
        when(examRepository.saveAndFlush(examMath)).thenReturn(persistedExam);

        Exam result = underTestInstance.saveExam(examMath);

        verify(examRepository, times(1)).saveAndFlush(examMath);
        assertEquals(persistedExam, result);
    }
}